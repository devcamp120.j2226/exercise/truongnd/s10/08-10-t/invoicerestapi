package com.devcamp.invoicerestapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InvoiceRestApiController {
    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList<InvoiceItem>  getListInvoiceItem(){
        InvoiceItem sanpham1 = new InvoiceItem(1, "Giay kien.", 4, 1000);
        InvoiceItem sanpham2 = new InvoiceItem(2, "Giay Bao.",5 , 1000);
        InvoiceItem sanpham3 = new InvoiceItem(3, "Giay Lot.", 6, 1000);

        System.out.println(sanpham1.toString());
        System.out.println(sanpham2.toString());
        System.out.println(sanpham3.toString());

        ArrayList<InvoiceItem> employeeItems = new ArrayList<>();
        employeeItems.add(sanpham1);
        employeeItems.add(sanpham2);
        employeeItems.add(sanpham3);

        return employeeItems;
    }

    
   

}
